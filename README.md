# Build instructions

- Requires : SBT & Scala 2.11
- Run the tests with: 'sbt test'
- Generate coverage report with: 'sbt jacoco:cover'

