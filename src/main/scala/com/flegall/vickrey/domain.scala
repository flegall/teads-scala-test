package com.flegall.vickrey

import scala.math.max

/**
 * An Offer made a buyer
 * @param buyer the buyer name
 * @param bids their bids
 */
case class Offer(buyer: String, bids: Seq[Double]) extends Ordered[Offer] {

  def maximumBid: Option[Double] = bids.reduceOption(_ max _)

  def compare(that: Offer): Int = {
    val thisSortedBids = this.bids.sorted.reverse.lift
    val thatSortedBids = that.bids.sorted.reverse.lift

    def getBestOffer(level: Int): Option[Offer] = {
      // These explicits allow comparing two Option[Double]
      import scala.math.Ordering.Implicits._

      if (thisSortedBids(level) > thatSortedBids(level)) {
        Some(this)
      } else if (thisSortedBids(level) < thatSortedBids(level)) {
        Some(that)
      } else {
        None
      }
    }

    val range = 0 until max(this.bids.length, that.bids.length)
    val result: Option[Offer] = range.foldLeft[Option[Offer]](None) { (acc, n) =>
      // Fold until we have a best offer
      if (acc.isDefined) {
        // If we already have an offer return it
        acc
      } else {
        // Else try to find one
        getBestOffer(n)
      }
    }

    result match {
      case None => 0
      case i: Some[Offer] if i.get == this => 1
      case i: Some[Offer] if i.get == that => -1
    }
  }
}

/**
 * An auction
 * @param reservedPrice the reserved price
 * @param offers a list of offers
 */
case class Auction(reservedPrice: Double, offers: Seq[Offer]) {

  def result: AuctionResult = {
    val firstOfferOption = bestOffer
    val secondOfferOption = secondBestOffer

    if (firstOfferOption.isDefined && secondOfferOption.isDefined) {
      val buyer = firstOfferOption.get.buyer
      val price = secondOfferOption.get.maximumBid.get

      if (price >= reservedPrice) {
        SomeResult(buyer, price)
      } else {
        SecondOfferIsTooLow
      }
    } else {
      TooFewOffers
    }
  }

  private[vickrey] def bestOffer: Option[Offer] = bestOffers.lift(0)

  private[vickrey] def secondBestOffer: Option[Offer] = bestOffers.lift(1)

  private def bestOffers: Seq[Offer] = offersAboveReservedPrice.sorted.reverse

  private def offersAboveReservedPrice: Seq[Offer] = {
    offers.filter(_.maximumBid.isDefined)
  }
}

sealed trait AuctionResult

/**
 * We have a winner
 * @param buyer the buyer name
 * @param price the price
 */
case class SomeResult(buyer: String, price: Double) extends AuctionResult

/**
 * We have too few acceptable offers
 */
case object TooFewOffers extends AuctionResult


/**
 * The second offer is too low
 */
case object SecondOfferIsTooLow extends AuctionResult
