package com.flegall.vickrey

import org.scalatest.{Matchers, FlatSpec}

class Auction_Spec extends FlatSpec with Matchers {
  val offerA = Offer("A", Seq(110, 130))
  val offerB = Offer("B", Seq.empty)
  val offerC = Offer("C", Seq(125))
  val offerD = Offer("D", Seq(105, 115, 90))
  val offerE = Offer("E", Seq(132, 135, 140))

  behavior of "Auction"

  it should "find the best offer in an auction" in {
    val auction = Auction(100, Seq(offerA, offerE))

    auction.bestOffer shouldBe Some(offerE)
  }

  it should "not find any best offer when nobody bids" in {
    val emptyOffer1 = Offer("A", Seq.empty)
    val emptyOffer2 = Offer("B", Seq.empty)
    val auction = Auction(100, Seq(emptyOffer1, emptyOffer2))

    auction.bestOffer shouldBe None
  }

  it should "find the second best offer" in {
    val auction = Auction(100, Seq(offerA, offerE))

    auction.secondBestOffer shouldBe Some(offerA)
  }

  it should "not find any the second best offer when there is none" in {
    val auction = Auction(100, Seq(offerE))

    auction.secondBestOffer shouldBe None
  }

  it should "find the result when there is one" in {
    val auction = Auction(100, Seq(offerA, offerE))

    auction.result shouldBe SomeResult("E", 130)
  }

  it should "not find any result when there are less than two offers" in {
    val auction = Auction(100, Seq(offerA))

    auction.result shouldBe TooFewOffers
  }

  it should "not find any result when the offers are below the reserved price" in {
    val auction = Auction(200, Seq(offerA, offerE))

    auction.result shouldBe SecondOfferIsTooLow
  }

  it should "find find the winner from the example data set" in {
    val auction = Auction(100, Seq(offerA, offerB, offerC, offerD, offerE))

    auction.result shouldBe SomeResult("E", 130)
  }

  it should "in case of two bidders with the same maximum bid, the bidder with the higest second bid should win" in {
    val offerA = Offer("A", Seq(140, 130, 120))
    val offerB = Offer("B", Seq(140, 135, 133))
    val auction = Auction(100, Seq(offerA, offerB))

    auction.result shouldBe SomeResult("B", 140)
  }
}
