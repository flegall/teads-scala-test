package com.flegall.vickrey

import org.scalatest.{FlatSpec, Matchers}

class Offers_Spec extends FlatSpec with Matchers {
  behavior of "Offers"

  it should "find the maximum bid of an offer" in {
    val offer = Offer("A", Seq(110, 130))

    offer.maximumBid shouldBe Some(130)
  }

  it should "find not find a maximum bid when an offer has no prices" in {
    val offer = Offer("A", Seq.empty)

    offer.maximumBid shouldBe None
  }

  it should "compare two offers according to highest different bids" in {
    val offerA = Offer("A", Seq(120, 130))
    val offerB = Offer("B", Seq(105, 110, 130))

    offerA compare offerB should be > 0
  }

  it should "compare two offers with different bids" in {
    val offerA = Offer("A", Seq(120, 130))
    val offerB = Offer("B", Seq.empty)

    offerA compare offerB should be > 0
  }

  it should "compare default offers" in {
    val offerA = Offer("A", Seq(110, 130))
    val offerB = Offer("B", Seq.empty)
    val offerC = Offer("C", Seq(125))
    val offerD = Offer("D", Seq(105, 115, 90))
    val offerE = Offer("E", Seq(132, 135, 140))

    val orderedOffers = Seq(offerA, offerB, offerC, offerD, offerE).sorted

    orderedOffers shouldBe Seq(offerB, offerD, offerC, offerA, offerE)
  }
}
